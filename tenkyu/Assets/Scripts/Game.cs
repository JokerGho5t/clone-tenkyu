﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }

    Transform Spawner = null;
    GameObject player = null;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Spawner = GameObject.FindGameObjectWithTag("Respawn").transform;
        Spawn();
    }

    private void Update()
    {
        if (transform.position.y < -100)
            Restart();
    }

    public void Spawn()
    {
        player.transform.position = Spawner.position;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public IEnumerator WaitRespawn()
    {
        yield return new WaitForSeconds(2);
        Restart();
    }
}
