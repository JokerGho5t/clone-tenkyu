﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float dumping = 10f;
    private Transform player;
    private Vector3 offset;

    private void Start()
    {
        FindPlayer();
    }

    private void FindPlayer()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        offset = transform.position - player.position;
    }

    private void Update()
    {
        if(player)
        {
            transform.position = Vector3.Lerp(transform.position, player.position + offset, dumping * Time.deltaTime);
        }
    }
}
