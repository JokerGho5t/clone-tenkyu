﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    Transform temp;

    Quaternion originRotation;
    bool flag = true;

    Vector2 firstPos;
    Vector2 secondPos;

    [Range(0,5f)]
    public float mouseSensX = 0.2f;
    [Range(0, 5f)]
    public float mouseSensY = 0.5f;

    public float viewAngleBack = 337f; //-23f
    public float viewAngleForward = 60f;
    public float viewAngleLeft = 45; //-45f
    public float viewAngleRight = 330;

    void FixedUpdate()
    {
        if (Input.touchCount > 0 && flag)
        {
            flag = !flag;
            firstPos = Input.touches[0].position;
            StartCoroutine(LevelRotator());
        }
    }

    private IEnumerator LevelRotator()
    {
        do
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                secondPos = touch.position;

                Vector2 mouseSpeed = new Vector2((firstPos.x - secondPos.x) / Camera.main.pixelWidth * 50, (firstPos.y - secondPos.y) / Camera.main.pixelHeight * 30);

                if (transform.rotation.eulerAngles.x <= 180)
                    mouseSpeed.x = (transform.rotation.eulerAngles.x + mouseSpeed.x >= viewAngleLeft) ? viewAngleLeft - transform.rotation.eulerAngles.x : mouseSpeed.x;
                else
                    mouseSpeed.x = (transform.rotation.eulerAngles.x + mouseSpeed.x <= viewAngleRight) ?  viewAngleRight - transform.rotation.eulerAngles.x : mouseSpeed.x;

                if (transform.rotation.eulerAngles.z <= 180)
                    mouseSpeed.y = (transform.rotation.eulerAngles.z + mouseSpeed.y >= viewAngleForward) ? viewAngleForward - transform.rotation.eulerAngles.z : mouseSpeed.y;
                else
                    mouseSpeed.y = (transform.rotation.eulerAngles.z + mouseSpeed.y <= viewAngleBack) ? viewAngleBack - transform.rotation.eulerAngles.z  : mouseSpeed.y;


                transform.RotateAround(Game.Instance.transform.position, Vector3.forward, mouseSpeed.x);

                originRotation = transform.rotation;
                Quaternion rotationZ = Quaternion.AngleAxis(mouseSpeed.y, Vector3.forward);
                transform.rotation = originRotation * rotationZ;

                firstPos = secondPos;
            }
            yield return new WaitForFixedUpdate();
        } while (Input.touchCount > 0);

        flag = !flag;
    }
}
